package com.hcodez.rma_lv2_prvi

import android.app.Application

class Rma_lv2_prvi : Application() {
    companion object {
        lateinit var application: Rma_lv2_prvi
    }

    override fun onCreate() {
        super.onCreate()
        application = this
    }
}