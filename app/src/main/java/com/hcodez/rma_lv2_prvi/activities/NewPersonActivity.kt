package com.hcodez.rma_lv2_prvi.activities

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.hcodez.rma_lv2_prvi.PeopleRepository
import com.hcodez.rma_lv2_prvi.R
import com.hcodez.rma_lv2_prvi.databinding.ActivityNewPersonBinding
import com.hcodez.rma_lv2_prvi.models.InspiringPerson

class NewPersonActivity : AppCompatActivity() {
    private lateinit var newPersonBinding: ActivityNewPersonBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_person)
        newPersonBinding = ActivityNewPersonBinding.inflate(layoutInflater)
        newPersonBinding.btnAddPerson.setOnClickListener { saveNote() }
        setContentView(newPersonBinding.root)
    }

    private fun saveNote() {
        val name = newPersonBinding.etName.text.toString().trim()
        val desc = newPersonBinding.etDescription.text.toString().trim()
        val birth = newPersonBinding.etBirth.text.toString().trim()
        val quote = newPersonBinding.etQuote.text.toString().trim()
        val scientist = InspiringPerson(name, name, birth, desc)

        if (name.isNotEmpty() && desc.isNotEmpty() && birth.isNotEmpty()) {
            PeopleRepository.insert(scientist)
            PeopleRepository.insertQuote(name, quote)
            finish()
        } else {
            Toast.makeText(this, "First tree fields are required!", Toast.LENGTH_LONG).show()
        }
    }
}